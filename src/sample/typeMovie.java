package sample;

public class typeMovie {
    private String nameMovie,time,type,imgPath,theater;

    public typeMovie(String nameMovie, String time, String type, String imgPath, String theater) {
        this.nameMovie = nameMovie;
        this.time = time;
        this.type = type;
        this.imgPath = imgPath;
        this.theater = theater;
    }

    public String getNameMovie() {
        return nameMovie;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getImgPath() {
        return imgPath;
    }

    public String getTheater() {
        return theater;
    }
}
