package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class Bill {
    @FXML
    Text costText,userNameText;
    private FormMovie movie;
    public void setMovie(FormMovie movie) {
        this.movie = movie;
    }

    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                setCost();

            }
        });
    }
    public void setCost() {
        costText.setText(movie.getPrice() + "");

    }
    public void logout(ActionEvent event){
        try {
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
            stage.setScene(new Scene(loader.load()));
//            MovieInfo movieInfo = loader.getController();
//            movieInfo.setMovie(movie);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
