package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    Button AcceptBtn;
    @FXML
    TextField UserField,passField;
    @FXML
    Text registerTxt,message;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        registerTxt.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    textRegister(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        AcceptBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    loginReaderFile(event);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public void textRegister(MouseEvent event) throws IOException {
        Text b = (Text) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("register.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }


    public void loginReaderFile(ActionEvent event)  {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("write.csv"));
            String line;
            boolean userTest = false;
            boolean passTest = false;

            while ((line = reader.readLine()) != null) {
                String[] arr = line.split(",");
                System.out.println(line);

                if (arr[3].equals(UserField.getText()) && arr[4].equals(passField.getText())){
                    userTest=true;
                    passTest=true;
                    break;
                }

                if (arr[3].equals(UserField.getText()) && !arr[4].equals(passField.getText())){
                    userTest=true;
                    break;
                }

            }
            reader.close();
            if (userTest==false){
                message.setText("Your Username is not correct!");
            }
            else if(passTest==false){
                message.setText("You Password is not correct!");
            }
            else {
                Confirm(event);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void Confirm(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("selected.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }
}
