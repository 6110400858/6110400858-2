package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class InfoMovie1  {
    @FXML
    Label nameText,typeText,timeText,theaterText;
    @FXML
    ImageView img;
    @FXML
    Button cancelBtn,time1,time2;
    @FXML
    Text desText;
    private FormMovie movie;
    public void setMovie(FormMovie movie) {
        this.movie = movie;
    }
    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                setMovieDetail();
                cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Button b = (Button) event.getSource();
                            Stage stage = (Stage) b.getScene().getWindow();
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("selected.fxml"));
                            //        Parent root = (Parent) loader.load();// loader.getController();
                            stage.setScene(new Scene(loader.load()));
                            stage.show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


    }
    public void setMovieDetail(){
        nameText.setText(movie.getName());
        timeText.setText(movie.getTime());
        typeText.setText(movie.getType());
        theaterText.setText(movie.getTheater());
        time1.setText(movie.getTimeRound());
        time2.setText(movie.getTimeRound2());
        desText.setText(movie.getDescribe());
        img.setImage(new Image(movie.getImgPath()));

    }
    public void choiceMovieRounding1(ActionEvent event){
        String a ;

        a = "theater47MeterRound1.fxml";


        try {
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource(a));
            stage.setScene(new Scene(loader.load()));
            typeController471 seatController1 = loader.getController();
            seatController1.setMovie(movie);
            stage.show();
        } catch (IOException e){
               e.printStackTrace();
        }


    }
    public void choiceMovieRounding2(ActionEvent event){
        String a ;
        switch (movie.getName()) {
            case "47Metrosdown":
                a = "theater47MeterRound2.fxml";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + movie.getName());
        }
        try {
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource(a));
            stage.setScene(new Scene(loader.load()));
            typeController472 seatController2 = loader.getController();
            seatController2.setMovie(movie);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
