package sample;

public class FormMovie {
    private String name,describe,time,type,imgPath,theater;
    private String timeRound,timeRound2;
    private int price = 0 ;

    public FormMovie(String name, String describe, String time, String type, String theater, String timeRound,String timeRound2,String imgPath) {
        this.name = name;
        this.describe = describe;
        this.time = time;
        this.type = type;
        this.theater = theater;
        this.timeRound = timeRound;
        this.timeRound2 = timeRound2;
        this.imgPath = imgPath;
    }

    public String getName() {
        return name;
    }

    public String getDescribe() {
        return describe;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getImgPath() {
        return imgPath;
    }

    public String getTheater() {
        return theater;
    }

    public String getTimeRound2() {
        return timeRound2;
    }
    public void setPrice(int price) {
        this.price += price;
    }
    public int getPrice() {
        return price;
    }

    public String getTimeRound() {
        return timeRound;
    }
}
