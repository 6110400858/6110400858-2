package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SelectedMovie implements Initializable {
    @FXML
    Button selectBtn,selectBtn1,selectBtn2,selectBtn3;
    @FXML
    ImageView m1,m2,m3,m4;
    private FormMovie[] formMovies = new FormMovie[4];
    public void info(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("info.fxml"));
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addDetailMovie();
        handleImgClick();
        handleImgClick1();
        handleImgClick2();
        handleImgClick3();
    }

    public void addDetailMovie(){
        formMovies[0] = new FormMovie("Aquaman","เป็นเรื่องราวของ “Arthur Curry” ชายหนุ่มที่ได้มารู้ที่มาที่ไปของตัวเองว่ามีสายเลือดของชาวแอตแลนติส  อาณาจักรใต้มหาสมุทรที่หายสาบสูญ  เขาเป็นลูกชายของ Thomas Curry พ่อที่เป็นคนดูแลประภาคารที่ชายฝั่งของอ่าว Amnesty Bay กับ Queen Atlanna แม่ที่เป็นราชินีแห่งอาณาจักรแอตแลนติสที่ถูกเนรเทศออกมา แต่หลังจากที่ Arthur ถือกำเนิดขึ้นได้เพียงไม่นาน Queen Atlanna ก็ได้หายตัวไป… ทำให้ Thomas พยายามออกตามหาภรรยาของตนเอง  ซึ่งแน่นอนว่าด้วยความที่ตัวเขามีพลังของชาวแอตแลนติส  เขาจึงได้ลุกขึ้นมาเพื่อเป็นฮีโร่และต่อสู้เพื่อมุ่งไปสู่การคอยปกป้องและนำทางผู้คนบนโลกใบนี้","142 Minutes","Normal cinema","Theater 1","9.00","12.50","images/aquman.jpg");
        formMovies[1] = new FormMovie("47Metrosdown","ณ ท้องทะเลอันสวยงาม และวัฒนธรรมอันโดดเด่นของประเทศบราซิล สาวสวย 5 คน ที่ต่างมีพื้นเพและเชื้อชาติที่ต่างกันได้มารวมตัวกันเพื่อเสาะหาความเร้าใจในการผจญภัย พวกเธอเริ่มเบื่อหน่ายกับเส้นทางท่องเที่ยวอันแสนธรรมดาประกอบกับการได้ทราบข่าวลือเรื่องซากปรักหักพังแห่งเมืองใต้สมุทร พวกเธอจึงไม่พลาดโอกาสที่จะออกไปค้นหาด้วยตัวเองเมืองซากปรักหักพังอันสวยงามที่ดูเหมือนจะถูกทอดทิ้งจมอยู่ใต้บาดาลอย่างไร้ค่า แต่ที่แห่งนี้ ไม่ได้ไร้ผู้อยู่อาศัย ยิ่งพวกเธอดำดิ่งลึกลงไปมากเท่าไหร่ยิ่งเป็นการรุกล้ำอาณาเขตของฉลามสายพันธุ์ที่ดุร้ายสุดแห่งท้องทะเล","90 Minutes","4K cinema","Theater 2","11.00","17.00","images/47meter.jpg");
        formMovies[2] = new FormMovie("ToyStory","เรื่องราวการผจญภัยครั้งใหม่ของ วู้ดดี้ ที่เริ่มต้นชีวิตในบ้านของเจ้าของคนใหม่ คือ บอนนี่ ซึ่งเธอได้ใช้ช้อนกึ่งส้อม ประดิษฐ์ออกมาเป็นของเล่นใหม่ที่ตั้งชื่อว่า ฟอร์คกี้ แต่เจ้าฟอร์คกี้ รู้ว่าแท้จริงเขาไม่ใช่ของเล่นแต่เป็นอุปกรณ์ครัว จึงอยากกลับไปสู่ชีวิตที่แท้จริง เดือดร้อนถึงวู้ดดี้ต้องตามกลับมา กลายเป็นการผจญภัยของเหล่าของเล่นครั้งใหม่ รวมถึงการกลับมาของโบ ของเล่นหวานใจของวู้ดดี้ที่เธอกลับมาในมาดใหม่เป็นสาวสุดแกร่งด้วย","100 Minutes","3D cinema","Theater 3","13.00","18.00","images/toy.jpg");
        formMovies[3] = new FormMovie("CaptainAmerica","  \"กัปตันอเมริกา : มัจจุราชอหังการ\" ภาพยนตร์แอ็คชั่นซูเปอร์ฮีโร่ที่แฟน ๆ รอคอยเรื่องล่าสุดจากมาร์เวลสตูดิโอ ซึ่งสานต่อการผจญภัยบนจอภาพยนตร์ของ \"สตีฟโรเจอร์ส\" หรือที่รู้จักในนาม \"กัปตันอเมริกา\" ในภาพยนตร์เรื่อง \"กัปตันอเมริกา : มัจจุราชอหังการ\" ดำเนินเรื่องหลังจากเหตุการณ์กลียุคในนิวยอร์กของเหล่า ดิ อเวนเจอร์สและการใช้ชีวิตเงียบ ๆ ของสตีฟโรเจอร์สในกรุงวอชิงตัน ดี.ซี. เพื่อพยายามที่จะปรับตัวเข้ากับโลกสมัยใหม่ ","136 Minutes","4K cinema","Theater 4","9.00","20.00","images/Captain_America.jpg");
    }
    @FXML
    public void changeScene(MouseEvent event, int i) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoMovie.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
            stage.setScene(new Scene(loader.load()));
            InfoMovie movieD = loader.getController();
            System.out.println(movieD);
            System.out.println(formMovies[i]);
            movieD.setMovie(formMovies[i]);
            stage.show();

    }
    @FXML
    public void changeScene1(MouseEvent event, int i) throws IOException {
        Button c = (Button) event.getSource();
        Stage stage = (Stage) c.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoMovie47Metrodown.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        InfoMovie1 movieD = loader.getController();
        System.out.println(movieD);
        System.out.println(formMovies[i]);
        movieD.setMovie(formMovies[i]);
        stage.show();

    }
    @FXML
    public void changeScene2(MouseEvent event, int i) throws IOException {
        Button d = (Button) event.getSource();
        Stage stage = (Stage) d.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoMovieToy.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        InfoMovie2 movieD = loader.getController();
        System.out.println(movieD);
        System.out.println(formMovies[i]);
        movieD.setMovie(formMovies[i]);
        stage.show();

    }
    @FXML
    public void changeScene3(MouseEvent event, int i) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoMovieCaptain.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        InfoMovie3 movieD = loader.getController();
        System.out.println(movieD);
        System.out.println(formMovies[i]);
        movieD.setMovie(formMovies[i]);
        stage.show();

    }

    public void handleImgClick() {
        selectBtn.setOnMouseClicked(event -> {
            try {
                changeScene(event, 0);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        }
        public void handleImgClick1() {
            selectBtn1.setOnMouseClicked(event -> {
                try {
                    changeScene1(event, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        public void handleImgClick2() {
        selectBtn2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    changeScene2(event, 2);
                } catch (IOException e) {
                    e.getMessage();
                }
            }
        });
    }
    public void handleImgClick3() {
        selectBtn3.setOnMouseClicked(event -> {
            try {
                changeScene3(event, 3);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

}
