package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class register {
    @FXML
    TextField nameTxt,lastNameTxt,usernameTxt,passwordTxt,EmailTxt;
    @FXML
    Button BackBtn,AcceptBtn;
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                AcceptBtn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            registerFile();
                            Accept(event);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                BackBtn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            registerFile();
                            Back(event);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });



            }
        });
    }
    public void registerFile() throws IOException {
        FileWriter fileWriter = new FileWriter("write.csv",true);
        BufferedWriter bw = new BufferedWriter(fileWriter);
        try {
            bw.append(nameTxt.getText());
            bw.append(",");
            bw.append(lastNameTxt.getText());
            bw.append(",");
            bw.append(EmailTxt.getText());
            bw.append(",");
            bw.append(usernameTxt.getText());
            bw.append(",");
            bw.append(passwordTxt.getText());
            bw.newLine();
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void Accept(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }
    public void Back(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
//        Parent root = (Parent) loader.load();// loader.getController();
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }
}
