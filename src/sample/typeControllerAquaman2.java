package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CopyOnWriteArrayList;

public class typeControllerAquaman2 implements Initializable {
    @FXML
    Button conFirmBtn,A1,A2,A3,A4,A5,B1,B2,B3,B4,B5,C1,C2,C3,C4,C5;
    @FXML
    ImageView m1Image;
    @FXML
    Label timeRound;

    private FormMovie movie;
    private List<String> arrListChairs = new CopyOnWriteArrayList<>();
    public void setMovie(FormMovie movie) {
        this.movie = movie;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        handleSeat();
        read();
    }
    public void save(){
        try {

            FileWriter fileWriter = new FileWriter("BookingDataAq2.csv",true);
            BufferedWriter bw = new BufferedWriter(fileWriter);
            for (int i =0; i<arrListChairs.size();i++){
                bw.write(arrListChairs.get(i)+"\n");
            }
                bw.close();
            } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void choiceMovieBack(ActionEvent event) {
        try {
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoMovie.fxml"));
            stage.setScene(new Scene(loader.load()));
            InfoMovie movieInfo = loader.getController();
            movieInfo.setMovie(movie);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void isShowtime(ActionEvent event){
        System.out.println(arrListChairs);
        try {
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("BookingData.fxml"));
            stage.setScene(new Scene(loader.load()));
//            MovieInfo movieInfo = loader.getController();
//            movieInfo.setMovie(movie);
            Bill bill = loader.getController();
            bill.setMovie(movie);
            save();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void read(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("BookingDataAq2.csv"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.trim().equals("A1")) { A1.setDisable(true); }
                if (line.trim().equals("A2")) { A2.setDisable(true); }
                if (line.trim().equals("A3")) { A3.setDisable(true); }
                if (line.trim().equals("A4")) { A4.setDisable(true); }
                if (line.trim().equals("A5")) { A5.setDisable(true); }
                if (line.trim().equals("B1")) { B1.setDisable(true); }
                if (line.trim().equals("B2")) { B2.setDisable(true); }
                if (line.trim().equals("B3")) { B3.setDisable(true); }
                if (line.trim().equals("B4")) { B4.setDisable(true); }
                if (line.trim().equals("B5")) { B5.setDisable(true); }
                if (line.trim().equals("C1")) { C1.setDisable(true); }
                if (line.trim().equals("C2")) { C2.setDisable(true); }
                if (line.trim().equals("C3")) { C3.setDisable(true); }
                if (line.trim().equals("C4")) { C4.setDisable(true); }
                if (line.trim().equals("C5")) { C5.setDisable(true); }
                System.out.println(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void handleSeat()  {
        A1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (A1.getOpacity() == 1.00) {
                    addChair(A1.getId());
                    A1.setDisable(true);

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }
                } else {
                    A1.setOpacity(1);
                    removeChair(A1.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }

            }
        });
        A2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (A2.getOpacity() == 1.00) {
                    A2.setOpacity(0.5);
                    addChair(A2.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else {
                    A2.setOpacity(1);
                    removeChair(A2.getId());
                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        A3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (A3.getOpacity() == 1.00) { A3.setOpacity(0.5);
                    addChair(A3.getId());
                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { A3.setOpacity(1);
                    removeChair(A3.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        A4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (A4.getOpacity() == 1.00) { A4.setOpacity(0.5);
                    addChair(A4.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }
                } else { A4.setOpacity(1);
                    removeChair(A4.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        A5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (A5.getOpacity() == 1.00) { A5.setOpacity(0.5);
                    addChair(A5.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { A5.setOpacity(1);
                    removeChair(A5.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        B1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (B1.getOpacity() == 1.00) { B1.setOpacity(0.5);
                    addChair(B1.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { B1.setOpacity(1);
                    removeChair(B1.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        B2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (B2.getOpacity() == 1.00) { B2.setOpacity(0.5);
                    addChair(B2.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }
                } else { B2.setOpacity(1);
                    removeChair(B2.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        B3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (B3.getOpacity() == 1.00) { B3.setOpacity(0.5);
                    addChair(B3.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { B3.setOpacity(1);
                    removeChair(B3.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        B4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (B4.getOpacity() == 1.00) { B4.setOpacity(0.5);
                    addChair(B4.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { B4.setOpacity(1);
                    removeChair(B4.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        B5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (B5.getOpacity() == 1.00) { B5.setOpacity(0.5);
                    addChair(B5.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { B5.setOpacity(1);
                    removeChair(B5.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        C1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (C1.getOpacity() == 1.00) { C1.setOpacity(0.5);
                    addChair(C1.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { C1.setOpacity(1);
                    removeChair(C1.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        C2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (C2.getOpacity() == 1.00) { C2.setOpacity(0.5);
                    addChair(C2.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { C2.setOpacity(1);
                    removeChair(C2.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        C3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (C3.getOpacity() == 1.00) { C3.setOpacity(0.5);
                    addChair(C3.getId());
                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { C3.setOpacity(1);
                    removeChair(C3.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
        C4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (C4.getOpacity() == 1.00) { C4.setOpacity(0.5);
                    addChair(C4.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                } else { C4.setOpacity(1);
                    removeChair(C4.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });C5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (C5.getOpacity() == 1.00) { C5.setOpacity(0.5);
                    addChair(C5.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }
                } else { C5.setOpacity(1);
                    removeChair(C5.getId());

                    if (movie.getName().equals("Aquaman")){
                        movie.setPrice(120);
                    }

                }
            }
        });
    }
    private void removeChair(String id) {
        arrListChairs.remove(id);
    }

    private void addChair(String id) {
        arrListChairs.add(id);
    }
}
